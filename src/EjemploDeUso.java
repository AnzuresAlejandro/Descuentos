
import Contolador.ColoniaCodigoPostal;
import Contolador.ColoniaCodigoPostalImplementacionArchivos;
import Contolador.DescuentosAdicionalesCodigoPostal;
import Contolador.DescuentosServiciosDBImplementacionArchivos;
import Contolador.FabricaCodigosPostales;
import Excepciones.ServicioInexistenteExcepcion;
import Modelo.CodigoPostal;
import Modelo.Colonia;
import Modelo.DescuentoAdicional;
import Modelo.DescuentosServicios;
import java.util.List;

public class EjemploDeUso {

    public static void main(String[] args) {
        FabricaCodigosPostales fabrica = new FabricaCodigosPostales();
        CodigoPostal codigo = fabrica.creaCodigoPostal("Mexico", "55717");
        ColoniaCodigoPostal obtenerColonia = new ColoniaCodigoPostalImplementacionArchivos();
        Colonia coloniaTemp = obtenerColonia.colonia(codigo);
        System.out.println(coloniaTemp);
        DescuentosServiciosDBImplementacionArchivos todos = new DescuentosServiciosDBImplementacionArchivos();
        try {
            List<DescuentosServicios> descuentos = todos.leerDescuentos();
            for (DescuentosServicios descuento : descuentos) {
                System.out.println("Todos los Descuentos:");
                System.out.println(descuento);
            }
            System.out.println("\n\n");
            DescuentosServicios s = descuentos.get(0);
            DescuentosAdicionalesCodigoPostal desAd = new DescuentosAdicionalesCodigoPostal();
            if (coloniaTemp != null) {

                if (DescuentosAdicionalesCodigoPostal.tieneDescuentoAdiccional(coloniaTemp.getCodigoPostal().getCodigoPostal(), s.getServicio())) {
                    System.out.print("El servicio "+s.getServicio().getNombreServicio()+" Tiene descuento Adicional y el total es :");
                    DescuentoAdicional descuentoFinal = desAd.regresaDescuentoAdicional(coloniaTemp.getCodigoPostal().getCodigoPostal(), s.getServicio(), s);
                    System.out.println(descuentoFinal.precioFinal());
                } else {
                    System.out.println("No tiene descuento Adicional pero el total es de"+ s.precioFinal());
                }
            } else {
                System.out.println("El codigo Postal no concuerda con alguan colonia registrada");
            }

        } catch (ServicioInexistenteExcepcion ex) {
            System.out.println(ex);
        }
    }

}
