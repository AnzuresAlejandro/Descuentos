package Contolador;

import Excepciones.ServicioInexistenteExcepcion;
import Modelo.DescuentoAdicional;
import Modelo.DescuentoFinal;
import Modelo.DescuentosServicios;
import Modelo.Servicio;


public class FabricaDescuentosCodigosPostales{

    public DescuentosServicios creaDescuento(String codigoPostal,String identificador, String establecimiento) throws ServicioInexistenteExcepcion {
        Servicio servicio = buscaServicio(identificador,establecimiento);
        DescuentoFinalCodigoPostal descuentoInicial = new DescuentoFinalCodigoPostal();
        DescuentoFinal descuentoActual = descuentoInicial.regresaDescuentoFinal(servicio);
        if(DescuentosAdicionalesCodigoPostal.tieneDescuentoAdiccional(identificador, servicio)){
            DescuentosAdicionalesCodigoPostal descuentoA = new DescuentosAdicionalesCodigoPostal();
            DescuentoAdicional descuento = descuentoA.regresaDescuentoAdicional(codigoPostal, servicio,descuentoActual);
            return descuento;
        }else{
            return descuentoActual;
        }        
    }

    private Servicio buscaServicio(String identificador, String establecimiento) throws ServicioInexistenteExcepcion {
        
        
        throw new ServicioInexistenteExcepcion("No existe ese descuento");
    }
}
