package Contolador;

import Excepciones.ServicioInexistenteExcepcion;
import Modelo.DescuentoFinal;
import Modelo.DescuentosServicios;
import Modelo.Servicio;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Clase que implementa la interfaz de DescuentosServiciosDB que se encarga de
 * inplementar la interfaz tomando como base de datos un archivo de texto
 */
public class DescuentosServiciosDBImplementacionArchivos implements DescuentosServiciosDB {

    @Override
    public List<DescuentosServicios> leerDescuentos() throws ServicioInexistenteExcepcion {
        FileReader lectorArchivo = null;
        try {
            String nombreArchivo = "./BaseDatos/Descuento/Descuento.txt";
            File archivo = new File(nombreArchivo);
            lectorArchivo = new FileReader(archivo);
            BufferedReader lector = new BufferedReader(lectorArchivo);
            String linea;
            DescuentosServiciosDBImplementacionArchivos buscaServicios = new DescuentosServiciosDBImplementacionArchivos();
            String nServicio = "";
            String nEstablecimiento = "";
            List<DescuentosServicios> descuentos = new ArrayList<>();
            DescuentosServicios descuento = new DescuentoFinal();
            while ((linea = lector.readLine()) != null) {
                if (linea.contains("categoria")) {
                    linea = linea.trim().substring(linea.indexOf(":") + 1).trim();
                    descuento.setCategoria(linea.trim());
                }
                if (linea.contains("servicio")) {
                    linea = linea.substring(linea.indexOf(":") +1 ).trim();
                    nServicio = linea.trim();
                }
                if (linea.contains("establecimiento")) {
                    linea = linea.substring(linea.indexOf(":")+1 ).trim();
                    nEstablecimiento = linea.trim();
                    descuento.setServicio(buscaServicio(nServicio, nEstablecimiento));
                    
                }
                if (linea.contains("descuento")) {
                    linea = linea.substring(linea.indexOf(":") + 1).trim();
                    descuento.setDescuento(Integer.parseInt(linea));
                    //descuento.setDescuento(0);
                    descuentos.add(descuento);
                    descuento = new DescuentoFinal();
                }

            }

            return descuentos;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(DescuentosServiciosDBImplementacionArchivos.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(DescuentosServiciosDBImplementacionArchivos.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                lectorArchivo.close();
            } catch (IOException ex) {
                Logger.getLogger(DescuentosServiciosDBImplementacionArchivos.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        throw new ServicioInexistenteExcepcion("No existen servicios");
    }

    public Servicio buscaServicio(String nombreServicio, String Establecimiento) throws ServicioInexistenteExcepcion {
        FileReader lectorArchivo = null;
        try {
            String nombreArchivo = "./BaseDatos/Establecimientos/Servicios.txt";
            File archivo = new File(nombreArchivo);
            lectorArchivo = new FileReader(archivo);
            BufferedReader lector = new BufferedReader(lectorArchivo);
            String linea;
//            System.out.println(nombreServicio);
//            System.out.println(Establecimiento);
            Servicio servicio = new Servicio();
            
            while ((linea = lector.readLine()) != null) {

                if (linea.contains("nombreServicio")) {
                    servicio.setNombreServicio(linea.substring(linea.indexOf(":") + 1));
                }
                if (linea.contains("costo")) {
                    servicio.setCosto(Double.parseDouble(linea.substring(linea.indexOf(":") + 1).trim()));
                }
                if (linea.contains("establecimiento")) {
                    String nombre = (linea.substring(linea.indexOf(":") + 1)).trim();
                    servicio.setEstablecimiento(nombre);
//                    System.out.println(nombre);
//                    System.out.println(Establecimiento);
                    String nombreEstablecimiento = Establecimiento.trim();
                    if (nombre.equalsIgnoreCase(nombreEstablecimiento)) {
                        
                        return servicio;
                    }
                    servicio = new Servicio();
                }
            }
            throw new ServicioInexistenteExcepcion("Servicios inexistentes");
        } catch (FileNotFoundException ex) {
            Logger.getLogger(DescuentosServiciosDBImplementacionArchivos.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(DescuentosServiciosDBImplementacionArchivos.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                lectorArchivo.close();
            } catch (IOException ex) {
                Logger.getLogger(DescuentosServiciosDBImplementacionArchivos.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        throw new ServicioInexistenteExcepcion("Servicios inexistentes");
    }

}
