package Contolador;

import Excepciones.ServicioInexistenteExcepcion;
import Modelo.DescuentosServicios;
import java.util.List;

public interface DescuentosServiciosDB {
    /**
     * Metodo que regresa una lista de todos los descuentos registrados en la Base de Datos descuentos en servicios
     * @return Lista de descuentos 
     */
    public List<DescuentosServicios> leerDescuentos() throws ServicioInexistenteExcepcion;
    
    
}
