package Contolador;

import Excepciones.ServicioInexistenteExcepcion;
import Modelo.DescuentoAdicional;
import Modelo.DescuentosServicios;
import Modelo.Servicio;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DescuentosAdicionalesCodigoPostal {

    public static boolean tieneDescuentoAdiccional(String codigoPostal, Servicio servicio) {
        FileReader lectorArchivo = null;
        try {
            String nombreArchivo = "./BaseDatos/Descuento/DescuentoAdicional.txt";
            File archivo = new File(nombreArchivo);
            lectorArchivo = new FileReader(archivo);
            BufferedReader lector = new BufferedReader(lectorArchivo);
            String linea;
            String nServicio = "";
            String nEstablecimiento = "";
            boolean condicion = false;
            while ((linea = lector.readLine()) != null) {
                if (linea.contains(codigoPostal)) {
                    condicion = true;
                }
                if (condicion && linea.contains("servicio")) {
                    linea = linea.substring(linea.indexOf(":") + 1).trim();
                    nServicio = linea.trim();
                }
                if (condicion && linea.contains("establecimiento")) {
                    linea = linea.substring(linea.indexOf(":") + 1).trim();
                    nEstablecimiento = linea.trim();
                    if (nServicio.trim().equalsIgnoreCase(servicio.getNombreServicio().trim())
                            && nEstablecimiento.trim().equalsIgnoreCase(servicio.getEstablecimiento().trim())) {
                        return true;
                    }
                }
            }
            return false;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(DescuentosServiciosDBImplementacionArchivos.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(DescuentosServiciosDBImplementacionArchivos.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                lectorArchivo.close();
            } catch (IOException ex) {
                Logger.getLogger(DescuentosServiciosDBImplementacionArchivos.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return false;
    }

    public DescuentoAdicional regresaDescuentoAdicional(String codigoPostal, Servicio servicio, DescuentosServicios descuentoInicial) throws ServicioInexistenteExcepcion {
        FileReader lectorArchivo = null;
        try {
            String nombreArchivo = "./BaseDatos/Descuento/DescuentoAdicional.txt";
            File archivo = new File(nombreArchivo);
            lectorArchivo = new FileReader(archivo);
            BufferedReader lector = new BufferedReader(lectorArchivo);
            String linea;
            DescuentoAdicional descuento = new DescuentoAdicional();
            String nServicio = "";
            String nEstablecimiento = "";
            String categoria = "";
            int descuentoA;
            boolean condicion = false;
            while ((linea = lector.readLine()) != null) {
                if (linea.contains(codigoPostal)) {
                    condicion = true;
                }
                if (condicion && linea.contains("servicio")) {
                    linea = linea.substring(linea.indexOf(":")+1 ).trim();
                    nServicio = linea.trim();
                }
                if (condicion && linea.contains("categoria")) {
                    linea = linea.substring(linea.indexOf(":")+1 ).trim();
                    categoria = linea.trim();
                }
                if (condicion && linea.contains("descuentoadicional")) {
                    linea = linea.substring(linea.indexOf(":")+1 ).trim();
                    descuentoA = Integer.parseInt(linea.trim());
                    
                    if (nServicio.trim().equalsIgnoreCase(servicio.getNombreServicio().trim())
                            && nEstablecimiento.trim().equalsIgnoreCase(servicio.getEstablecimiento().trim())) {
                        descuento.setServicio(servicio);
                        descuento.setCategoria(categoria);
                        descuento.setDescuentoAdiccional(descuentoA);
                        descuento.setDescuento(descuentoInicial.getDescuento());
                        return descuento;
                    }
                }
                if (condicion && linea.contains("establecimiento")) {
                    linea = linea.substring(linea.indexOf(":")+1 ).trim();
                    
                    nEstablecimiento = linea.trim();
                }

            }

            return descuento;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(DescuentosServiciosDBImplementacionArchivos.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(DescuentosServiciosDBImplementacionArchivos.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                lectorArchivo.close();
            } catch (IOException ex) {
                Logger.getLogger(DescuentosServiciosDBImplementacionArchivos.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        throw new ServicioInexistenteExcepcion("Este  servicio no contiene un descuento adicional");
    }
}
