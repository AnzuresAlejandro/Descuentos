package Contolador;

import Excepciones.ServicioInexistenteExcepcion;
import Modelo.DescuentoFinal;
import Modelo.Servicio;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DescuentoFinalCodigoPostal {
    public DescuentoFinal regresaDescuentoFinal(Servicio servicio) throws ServicioInexistenteExcepcion{
        FileReader lectorArchivo = null;
        try {
            String nombreArchivo = "./BaseDatos/Descuento/Descuento.txt";
            File archivo = new File(nombreArchivo);
            lectorArchivo = new FileReader(archivo);
            BufferedReader lector = new BufferedReader(lectorArchivo);
            String linea;
            DescuentoFinal descuento = new DescuentoFinal();
            String nServicio = "";
            String nEstablecimiento = "";
            String categoria = "";
            int descuentoA;
            boolean condicion = false;
            while ((linea = lector.readLine()) != null) {
                if (condicion && linea.contains("servicio")) {
                    nServicio = linea.trim();
                }
                if (condicion && linea.contains("categoria")) {
                    categoria = linea.trim();
                }
                if (condicion && linea.contains("descuento")) {
                    descuentoA = Integer.parseInt(linea.trim());
                    if (nServicio.trim().equalsIgnoreCase(servicio.getNombreServicio().trim())
                            && nEstablecimiento.trim().equalsIgnoreCase(servicio.getEstablecimiento().trim())) {
                        descuento.setServicio(servicio);
                        descuento.setCategoria(categoria);
                        descuento.setDescuento(descuentoA);
                    }
                }
                if (condicion && linea.contains("establecimiento")) {
                    nEstablecimiento = linea.trim();
                }

            }
            return descuento;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(DescuentosServiciosDBImplementacionArchivos.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(DescuentosServiciosDBImplementacionArchivos.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                lectorArchivo.close();
            } catch (IOException ex) {
                Logger.getLogger(DescuentosServiciosDBImplementacionArchivos.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        throw new ServicioInexistenteExcepcion("Este  servicio no contiene un descuento adicional");
    }
}
